# Segmentflow
<!------------------------------------------------------------------------>
Developed by C. Gus Becker (GitHub: [@gusbusdraws](https://github.com/GusBusDraws/), GitLab: @cgusb).

Segmentflow is a Python package that makes it easy to establish image
segmentation workflows, especially for generating voxel and surface mesh
geometries for 3D image data obtained from processes like x-ray computed
tomography (CT). Segmented data can be exported in a variety of formats
including collections of binary images, integer-labeled voxels (integer
value of pixels corresponds to unique particles) or collections of STL files.

## Contents
<!------------------------------------------------------------------------>
1. [Requirements](#requirements)
1. [Typical Routine](#typical-routine)
1. [Testing segmentation parameters](#testing-segmentation-parameters)
1. [Running Segmentflow on Alpine](#running-segmentflow-on-alpine)
1. [Regression Testing](#regression-testing)
1. [Change log](#change-log)

## Requirements
<!------------------------------------------------------------------------>
Required for install:
- Python >= 3.5
- imageio >= 2.21.0
- matplotlib >= 3.5.2
- NumPy >= 1.23.1
- numpy-stl >= 2.17.1
- pandas >= 1.4.3
- PyYAML >= 6.0
- scikit-image >= 0.19.3
- SciPy >= 1.9.0

Required for `mesh` submodule:
- Open3d >= 0.15.1

## Getting Started
<!------------------------------------------------------------------------>
It's recommended to install Segmentflow as a Python package in editable mode
with pip by cloning the repository, activating a virtual environment,
navigating to the root directory of the repository, and using the command:
```bash
python -m pip install -e path/to/segmentflow
```
This will allow you to pull updates to Segmentflow from GitLab and use the
updated package without uninstalling and re-installing the package.

There are three ways to run Segmentflow:
1. Use the Segmentflow API to write a Python script or Jupyter notebook,
2. Execute the default `workflow` submodule as a script with a YAML input
   file, or
3. Use a workflow script/input file combination from `./workflows/`.

To execute the a Segmentflow workflow `WORKFLOW_NAME`, execute the workflow
with an input file as follows:
```bash
python -m segmentflow.workflows.WORKFLOW_NAME -i path/to/input.yml
```
Note that each workflow file will have a different input file.

## Typical routine

### Input Loading
<!------------------------------------------------------------------------>
All inputs are stored in a separate YAML file for ease of use and
reproducibility. With inputs stored in a separate file, the input used in
one run can be reused or slightly altered in future run, while also keeping
a record of what may or may not have worked in previous runs. A loading
function parses the parameters into a dictionary. Any blank parameters are
replaced with default values stored in the loading function. After loading,
a copy of the YAML inputs are saved in another YAML file in the STL output
directory specified. This reduces the likelihood that the file will be
edited while also back-filling default values for parameter values not
provided in the initial YAML.

### Preprocessing
<!------------------------------------------------------------------------>
Image preprocessing steps include median filter application and intensity
clipping. Applying a median filter to the data reduces noise retaining
edges (unlike Gaussian filtering which will blur edges). Intensity
clipping happens by setting an upper and lower threshold define by
intensity percentile and rescaling the data to that range. Each clipped
intensity is replaced by the value at the bounds of the clip.

### Binarization
<!------------------------------------------------------------------------>
Image binarization is performed by applying a multi-Otsu threshold
algorithm to generate threshold values which divide an image into N
regions. This is done by maximizing inter-class variance.

### Segmentation
<!------------------------------------------------------------------------>
Image segmentation is performed by calculating a distance map from the
binary images which maps the distance to the nearest background pixel to
each foreground pixel. Local maxima are calculated based on a minimum
distance (aligned with minimum particle size) and are used to seed a
watershed segmentation which "floods" the inverted distance map starting
with the seed points as "pouring locations". Can also be thought of as
growing outwards from the seed points. Result of segmentation is a series
of images representing the same volume loaded with the input slice, row,
and column crops. Pixels in the segmented region are 0 for background and
an integer ID if the pixel belongs to a segmented particle. Each particle
has a unique ID ranging from 1 to N particles segmented.

### Surface Meshing
<!------------------------------------------------------------------------>
Surface meshes are created for the segmented particles using a marching
cubes algorithm implemented in scikit-image. There are some voxel
processing methods available before surface meshing is performed such
as voxel smoothing and particle erosions. In voxel smoothing, a median
filter is applied to the voxels of an isolated, binarized particle such
that each voxel is replaced by the median value (0 or 1) of the surrounding
26 voxels (3x3x3 cube). The voxels can also be subject to a series of
morphologic erosions, in which the outer layer of voxels is removed,
similar to the peeling of an onion. After these steps, the marching cubes
algorithm is applied with a specified voxel step size which determines the
granularity of the surface mesh.
The surface meshes output from the marching cubes algorithm are blocky
due to the limited number of surface normals output from the data. Normals
can take the form of each of the 6 Cartesian vectors for voxels on a flat
surface of the particles, as well as the 12 vectors halfway between
each of the Cartesian directions for voxels on the corners, and the 8
vectors between each set of three connecting edges for the corner voxels.

### Mesh Postprocessing
<!------------------------------------------------------------------------>
Mesh postprocessing steps consist of either Laplacian smoothing of the
mesh and/or mesh simplification to reduce the number of triangles/surface
elements. Smoothing the blocky surface meshes output by the marching cubes
algorithm can result in meshes that are more similar to the particles in
reality. These meshes may still have a large number of surface elements
however because the smoothing operation does not change the number of
triangles. To reduce the number of triangles, simplification can be
performed by providing a target number of triangles to scale down the mesh.
This can be done in a single step, or by iteratively reducing the number
of triangles by a specified factor.

### Outputs
<!------------------------------------------------------------------------>
Segmentflow outputs STL files for each particle segmented according to the
provided input parameters. In addition to these STL files, a copy of the
input parameter YAML file (with blank values backfilled with default values)
and a properties CSV file that includes details about each particle.
Currently, the properties CSV includes:

- ID
- Number of voxels
- Centroid (x, y, z)
- Minimum slice bounds
- Maximum slice bounds
- Minimum row bounds
- Maximum row bounds
- Minimum column bounds
- Maximum column bounds

[Back to top](#segmentflow)

## Testing segmentation parameters

The module [workflow.test_parameters](segmentflow/workflows/test_parameters.py)
is designed to determine the right parameters to use to perform a segmentation
on a CT scan using Segmentflow, by running a routine that takes only a couple
minutes before performing a full instance segmentation which can take much
longer to run, depending on the size of the CT scan. The associated input file
([test_parameters.yml](input_files/test_parameters.yml)) has the following
parameters:

A. Input
- A.1 Input dir path
- A.2 File suffix
- A.3 Slice crop
- A.4 Row crop
- A.5 Column crop

B. Preprocessing
- B.1 Apply radial filter
- B.2 Apply median filter
- B.3 Rescale intensity range

C. Semantic segmentation
- C.1 Histogram bins for calculating auto thresholds
- C.2 Size of smoothing kernel for histogram peak ID
- C.3 Upper and lower y-limits of histogram
- C.4 Manual threshold values (if None, auto used)
- C.5 Perform semantic segmentation
- C.6 Fill holes in semantic segmentation
- C.7 Reduce noise in semantic segmentation

D. Output
- D.1 Path to save output dir
- D.2 Output prefix & dir name
- D.3 Overwrite files
- D.4 Checkpoint plots n slices
- D.5 Specific slices to plot

The `A.*` parameters refer to the input,
`B.*` parameters to the preprocessing
`C.*` parameters to the semantic segmentation
(i.e. determining/testing thresholds to separate void, binder, and grains),
and `D.*` parameters to the output.
These mostly match the parameters inside workflows like
[workflows.IDOX_pucks](segmentflow/workflows/IDOX_pucks.py)
(though excludes parameters related to instance segmentation and surface mesh
creation).

The first and most obvious things to change are the related to file
paths: `A.1`, `A.2`, `D.1` and `D.2`. After changing the file paths, the images
themselves should be briefly examined to determine the cropping (`A.3-5`).
If the slices towards the top & bottom of the sample do not appear as circles,
the sample may be tilted along the Z axis. This can be corrected by cropping
the slices to the points where a full, uniform circle is visible.

For the preprocessing steps, the radial filter toggle (`B.1`) corrects
artifacts associated with beam hardening effects, though can also cause
arc-like artifacts of their own around the outer ring of the sample, so this
is a balance. Eventually I would like to include an option to crop out the
external few concentric circles of a sample, but this is not yet a feature.
The fitted circle radius & centroid is out in the log file though,
in case someone is feeling up to the task!
The median filter toggle (`B.2`) is typically useful to include as
it reduces noise in the CT scans. Rescaling the intensity of the histogram by
passing a list of two values to `B.3` is important to get an adequate contrast
for the semantic segmentation step. These values are percentiles, so any float
in the range [0, 100] will work! After running the workflow, you can check the
values in the output 'hist-intensity-rescale-vals' plot:

![hist-intensity-rescale-vals](images/02-hist-intensity-rescale-vals.png)

There are two main ways to determine the threshold values used to semantically
segment the material classes in CT scans of composites: calculate the
thresholds from dips in the CT histogram, or manually set the values by
visually inspecting the histogram. Typically I use the calculated values as a
first step even when ultimately passing them manually. To calculate some
potential values, the histogram must be smoothed with a kernel provided by
`C.2`. Sometimes a small kernel of 3 for only the direct neighbors works, but
for noisy scan, values around 250 may be necessary. You can see how this is
working by checking the smoothed histogram in the output 'hist-semantic-seg'
plot:

![hist-semantic-seg](images/05-hist-semantic-seg.png)

In some cases, a threshold may only be calculated between large peaks.
In the above example, the largest peak corresponds to the binder between
grains, the smaller peak with a slightly higher intensity corresponds
to the brighter grains, and the large, but narrow peak at the lowest
intensities corresponds to the void space. Since a threshold isn't detected
between the void and binder, a value can be chosen manually while the
also passing the calculated threshold between the binder and grain peaks
to `C.4`:

![hist-manual-thresh](images/06-hist-manual-thresh.png)

Once the thresholds look promising, they can be tested by toggling `C.5` to
`True`. This performs the semantic segmentation, which will separate the
voxels in the CT scan to different classes between each threshold. In the
case of the example, this means void (purple), binder (blue), and grain
(yellow):

![semantic-seg-imgs](images/07-semantic-seg-imgs.png)

Once the semantic segmentation is looking good, these same parameters can be
used in another workflow
(like [workflow.IDOX_pucks](segmentflow/workflows/IDOX_pucks.py))
to perform the instance segmentation that will label individual particles
(grains) and save labeled voxels and/or surface meshes.

[Back to top](#segmentflow)

## Regression Testing
<!------------------------------------------------------------------------>
Regression tests are grouped in a class in the
[pytest](https://docs.pytest.org/en/7.4.x/contents.html) style, and can be run
with pytest using the following command:
```
pytest -q tests/test_semantic_to_stl.py
```
The script can also be run on its own as a Python module:
```
python -m tests.test_semantic_to_stl
```

## Running Segmentflow on Alpine

The workflow [grainstats_mpi.py](segmentflow/workflows/grainstats_mpi.py) was
developed to run in parallel using the Python package `mpi4py`. This can be
run in an interactive session on the CU supercomputer using the following
instructions.

### Setting up the environment

```shell
module purge
module load anaconda/2023.09
module load intel/2024.2.1
module load impi/2021.13

conda create -n mpi4py_env
conda activate mpi4py_env
env MPICC=$(which mpicc) CC=$(which mpicc) python -m pip install --upgrade --no-cache-dir mpi4py
```


### Running Segmentflow in parallel

Start an interactive session with four processes on the same node for an hour:

```shell
sinteractive --partition=atesting --ntasks=4 --time=01:00:00 --nodes=1
```

Load the required modules and activate the Anaconda environment:

```shell
module load anaconda/2023.09
module load intel/2024.2.1
module load impi/2021.13
conda activate mpi4py_env
```

The install of `mpi4py` can be tested using the following command:

```shell
mpiexec -n 4 python -m mpi4py.bench helloworld
```

Segmentflow can be run with multiple processes using this command:

```shell
mpiexec -n 4 python -m segmentflow.workflows.grainstats_mpi -i path/to/INPUT_FILE.yml
```

[Back to top](#segmentflow)

## Change log
List of major changes for each version in reverse chronological order.

### 0.0.5
- 2025-02-28: Update README with [Alpine instructions](#running-segmentflow-on-alpine)
- 2025-02-28: Add `branch_vectors_to_fabric` to [grainstattools.py](segmentflow/grainstattools.py)
- 2025-02-25: Add [workflows.workflow_mpi.py](segmentflow/workflows/workflow_mpi.py)
- 2025-02-25: Add `parallel_setup` method to [workflows.grainstats_mpi.py](segmentflow/workflows/grainstats_mpi.py)
- 2025-02-24: Update method for collecting file ID based on pathlib methods in `grainstats.grainstattools.generate_stats_mpi` and `grainstats.grainstattools.generate_stats`
- 2025-02-24: Rename `Grain_stats` class -> `Grainstats_MPI` and move up parent class initialization in [workflows.grainstats_mpi.py](segmentflow/workflows/grainstats_mpi.py)
- 2025-02-21: Add `trimesh` to requirements
- 2025-02-21: Update `workflows.grainstats` input parameters
- 2025-02-18: Log fitted circle information in `segment.radial_filter`
- 2025-02-18: Improve consistency between `workflow.IDOX_pucks` and `workflow.test_parameters`
- 2025-02-18: Rename IDOX_pucks_input.yml -> IDOX_pucks.yml
- 2025-02-18: Add radial filter to `workflow.test_parameters`
- 2025-02-17: Add option to skip regions in `segment.save_as_stl_files` for `workflow.IDOX_pours`
- 2025-02-17: Add `grainstattools.generate_stats_mpi`
- 2025-02-13: Combine structure of MPI code with `grainstats.py` OOP workflow
- 2025-02-13: Add MPI code for splitting up STL files to `grainstats_mpi.py`
- 2025-02-13: Add `workflow.labels_resegment` for further segmenting a single label
- 2025-02-13: Restructure `workflow.IDOX_pours` to OOP
- 2025-02-03: Add `hist_smooth_size` kwarg to `segment.threshold_multi_min`
- 2025-02-03: Add `workflow.test_parameters`
- 2025-02-03: Add logger to `segment.load_inputs`
- 2025-02-03: Update docstring format to fix VS Code preview
- 2024-11-22: Add logging to `watertight_fraction` and `watertight_volume` in [view.py](segmentflow/view.py)
- 2024-11-22: Add logging to [IDOX_pucks](segmentflow/workflows/IDOX_pucks.py)
- 2024-11-22: Add log functionality to `mesh.save_stl` and `mesh.postprocess_meshes`
- 2024-11-22: Add log functionality to `segment.create_surface_mesh`, `segment.calc_voxel_stats`, `segment.fill_holes`, `segment.save_as_stl_files`, and `segment.save_images`
- 2024-09-12: Add logger to `view.histogram`
- 2024-09-12: Add logger to `segment.preprocess_images`
- 2024-09-11: Refactor [IDOX_pucks](segmentflow/workflows/IDOX_pucks.py) into a class
- 2024-09-11: Replace `print` with `log` in `segment.radial_filter`
- 2024-08-08: Update version in setup.py to 0.0.5
- 2024-08-08: Add `radial_filter` to [segment.py](segmentflow/segment.py)
- 2024-08-08: Add radial filter to [IDOX_pucks](segmentflow/workflows/IDOX_pucks.py)
- 2024-08-08: Increment version to 0.0.5
- 2024-06-13: Output coordinates adjusted with spatial resolution in `segment.save_bounding_coords` and`segment.save_bounding_boxes`
- 2024-06-13: Fix bug that would occasionally skip the first coordinate when smoothing in `segment.smooth_bounding_coords`
- 2024-06-11: Add input file [grainstats.yml](input_files/grainstats.yml) to pair with [segmentflow.workflows.grainstats](segmentflow/workflows/grainstats.py)
- 2024-06-11: Move super init after basic name/description/input initializations so `generate_input_file` and `help` functions work properly in [segmentflow.workflows.grainstats](segmentflow/workflows/grainstats.py)
- 2024-06-11: Compile order of GrainStatTools operations into [segmentflow.workflows.grainstats](segmentflow/workflows/grainstats.py)
--2-024-06-11: Add functions from GrainStatTools to [segmentflow.grainstattools](segmentflow/grainstattools.py)
- 2024-06-11: Add [segmentflow.workflows.grainstats](segmentflow/workflows/grainstats.py)
- 2024-06-11: Add [segmentflow.grainstattools](segmentflow/grainstattools.py)
- 2024-06-11: Fix bug causing in skipped regions in [sem_outlines](segmentflow/workflows/sem_outlines.py) workflow
- 2024-06-10: Add region smoothing option to [sem_outlines](segmentflow/workflows/sem_outlines.py) workflow
- 2024-06-06: Update [sem_outlines](segmentflow/workflows/sem_outlines.py) workflow to sort points by nearest neighbor instead of increasing polar angle
- 2024-06-06: Add logger info messages to `segment.watershed_segment`
- Add logger info messages to `segment.save_binned_particles_csv`
- Add logger info messages to `segment.simulate_sieve_bbox`
- Add logger info message to `segment.get_dims_df`
- Add logger info messages to `segment.load_images`
- Add [Workflow](segmentflow/workflows/Workflow.py) class for specific workflows to inherit

### 0.0.4
- Add 'remove_particles' option to [IDOX_pucks](segmentflow/workflows/IDOX_pucks.py)
- Add `segment.remove_particles` for removing particles smaller than a set volume.
- Add 'return_image' kwarg for `view.color_labels`
- Add ability to save binned particles in [labels_to_size](segmentflow/workflows/labels_to_size.py)
- Add `segment.save_binned_particles_csv` to save CSV of particles numbers
- Add F50 and IDOX standard size distribution to `view.grading_curve`
- Add F50 and IDOX standard bin edges to `segment.simulate_sieve_bbox`
- Update `view.grading_curve` to work with `segment.simulate_sieve_bbox` and `segment.get_dims_df`
- Add `segment.simulate_sieve_bbox` for size distribution workflow
- Add `segment.get_dims_df` for size distribution workflow
- Make [labels_to_size](segmentflow/workflows/labels_to_size.py) workflow object oriented
- Add `segment.save_shell_vertices` for visualization purposes
- Add option to exclude top and bottom bounding slices in `view.vol_slices` and `view.color_labels`
- Update `view.vol_slices` and `view.color_labels` to handle single slices
- Remove 16-bit scaling from `segment.threshold_multi_min` and `segment.threshold_multi_otsu`
- Add `watertight_volume()` to [view.py](segmentflow/view.py) for chart weighted by volume of all particles and [IDOX_pours](segmentflow/workflows/IDOX_pours.py)
- Add `watertight_fraction()` to [view.py](segmentflow/view.py) and [IDOX_pours](segmentflow/workflows/IDOX_pours.py)
- Add `save_vtk()` to [segment.py](segmentflow/segment.py)
- Add overwrite option to `save_images()` in [segment.py](segmentflow/segment.py)
- Fix bug in [single_grain](segmentflow/workflows/single_grain.py) where more than largest particle is saved
- Add "flip in z" step to [single_grain](segmentflow/workflows/single_grain.py)
- Update [single_grain](segmentflow/workflows/single_grain.py) to have voxel median filtering
- Add `stl_is_watertight` and `stl_volume` to outputted properties CSV
- Replace `view.slices()` calls with `view.vol_slices()` in [IDOX_CHESS](segmentflow/workflows/IDOX_CHESS.py)
- Add fill holes and semantic median filter to [IDOX_pours](segmentflow/workflows/IDOX_pours.py)
- Add numbers and letters to [IDOX_pours](segmentflow/workflows/IDOX_pours.py) input file
- Add base erosion of 1 to [single_grain](segmentflow/workflows/single_grain.py) workflow to account for marching cubes mesh wrapping around voxels
- Add voxel step size option to [single_grain](segmentflow/workflows/single_grain.py) workflow
- Update [test_semantic_to_stl.py](tests/test_semantic_to_stl.py) to work with pytest
- Add [test_semantic_to_stl.py](tests/test_semantic_to_stl.py)
- Update [semantic_to_stl.py](segmentflow/workflows/semantic_to_stl.py) to OOP with Workflow class
- Remove `tests/`from `.gitignore`
- Rename F50_single_grain_segment files to single_grain since they work with F50 and IDOX- Rename 'view.slices()' to 'view.vol_slices()' to avoid error with kwarg also called 'slices'
- Rename 'view.slices()' to 'view.vol_slices()' to avoid error with kwarg also called 'slices'

### 0.0.3
- Add `IDOX_pucks` workflow
- Add output_checkpoints to `IDOX_pours` workflow
- Update `instance_to_stl` workflow with ability to exclude border particles
- Update `view.hist()` with ability to mark values on plot
- Add print statement for generating histogram in `view.histogram()`
- Add `segment.fill_holes()` to [IDOX_CHESS](segmentflow/workflows/IDOX_CHESS.py) workflow
- Add `segment.fill_holes()` for filling holes in semantic-segmented images
- Add workflow [instance_to_stl.py](segmentflow/workflows/IDOX_CHESS.py)
- Add workflow [IDOX_CHESS.py](segmentflow/workflows/IDOX_CHESS.py)
- Wrap checkpoint show/save logic into function `output_checkpoints()`
- Add `color_labels()` as alternative to `plot_color_labels()` and fix image slicing logic
- Add 'n_voxels_post_erosion' column to properties.csv to quantify volume change following erosion
- Add STL min/max to properties.csv saved in `segment.save_as_stl_files()` to verify matching dimensions to input voxels
- Return STL vectors from `segment.create_surface_mesh()`
- Rename F83_01_segment.py to [F82_segment.py](segmentflow/workflows/F83_segment.py)
- Add checkpoint images & printed voxel stats to [F83_01_segment.py](segmentflow/workflows/F83_segment.py)
- Add STL viewing capability to [segmentflow.view](segmentflow/view.py)
- Add workflow script [semantic_to_stl.py](segmentflow/workflows/semantic_to_stl.py)
- Update `view.plot_slices()` to plot last slice when prompted with keyword arg `nslices`
- Add workflow script [F50_single_grain_segment.py](segmentflow/workflows/F50_single_grain_segment.py)
- Update `mesh.prostprocess_meshes()` to allow first STL to be skipped (in the case of first STL corresponding to binder).
- Add workflow script [postprocess_stls.py](segmentflow/workflows/postprocess_stls.py)
- Add `segmentflow.segment.calc_voxel_stats()` for determining binder to particle voxel ratios
- Add workflow script [labels_to_stl.py](segmentflow/workflows/labels_to_stl.py)
- Added version log to README
- Fix polluted namespace in which `stl.mesh` imported as `mesh`, conflicting with `segmentflow.mesh` imported as `mesh`.

[Back to top](#segmentflow)

