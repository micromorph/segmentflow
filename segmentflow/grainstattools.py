import cvxpy as cvx
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from scipy import spatial
from stl import mesh


def analyze_stats(grainstats_csv_path, logger=None):
    output_dir_path = Path(grainstats_csv_path).parent
    output_file_path = Path(output_dir_path) / 'grainstats_summary.csv'
    # Sharing (8 slices)
    # cuts = np.linspace(0,2*np.pi, 9, endpoint = True)
    # Whole pie to myself (1 slice)
    cuts = np.linspace(0,2*np.pi, 2, endpoint = True)
    normalize_by_mass = True
    # If only one slice, add "whole_pie" suffix to output file
    if len(cuts) == 2:
        output_file_path = str(output_file_path).replace(
            '.csv', '_whole_pie.csv')
        log(logger, 'Only one slice - "_whole_pie" appended to output fn')
    if normalize_by_mass:
        output_file_path = str(output_file_path).replace(
            '.csv', '_normalized.csv')
        log(logger, 'Normalizing by mass - "_normalized" appended to output fn')
    slices = np.stack((cuts[:-1], cuts[1:]))
    filenames = [grainstats_csv_path]
    # Repeat each row of slices for each file, and repeat each file for each
    # set of slices (outer product)
    n_filenames = len(filenames)
    filenames *= (len(slices.T))
    slices = np.repeat(slices, n_filenames, axis=1)
    # Write file header
    with open(output_file_path, 'w') as f:
        f.write("file,slice_start,slice_end,"
                "T11,T22,T33,T23,T13,T12,"
                "uniformity,gradation,"
                "particles,number of branch vectors,"
                "D10,D30,D60\n")
    # The method for finding particles in contact with one-another.
    # construct a distance matrix and check its entries against the radii of
    # the two involved particles
    branch_vector_method = '2-radius'
    # These are generally trash, but may be better for untested cases
    # branch_vector_method = 'ball'
    # branch_vector_method = 'knn'
    for i,filename in enumerate(filenames):
        # csv file from the minimum enclosing ellipsoid of the previous
        # notebook, over all the particles
        log(logger, f'Analyzing file: {filename}, i={i}/{len(filenames)-1}')
        slice_i = slices[:,i]
        log(logger, f'Slice: {slice_i}')
        # Read the csv file: skip the first row and the first column
        # Just do this every iteration, as we rewrite data every time, and we
        # might change filenames...
        data = np.genfromtxt(filename, delimiter=',', skip_header=1)
        data = data[:,1:]
        violation = data[:,-1]
        # Remove outliers. Constraint should be of order 1, so this is
        # reasonable.
        acceptable_violation = 5e-2
        remove_inds = np.abs(violation) < acceptable_violation
        log(logger,
            f'Number of outliers: {np.sum(~remove_inds)} / {len(remove_inds)}')
        data = data[remove_inds,:]
        xms, dms = calculate_ellipse_essentials(data)
        # Slicing
        xcenter = np.mean(xms, axis=0)
        xms_c = xms - xcenter
        theta = np.arctan2(xms_c[:,1], xms_c[:,0]) % (2*np.pi)
        idx = np.logical_and(theta >= slice_i[0], theta <= slice_i[1])
        xms = xms[idx,:]
        dms = dms[idx,:]
        n = xms.shape[0]
        print(f'{dms.shape=}')
        np.save(Path(output_dir_path) / 'grainstats_dms.npy', dms)
        msg = (
            f"Array containing data saved:"
            f" {Path(output_dir_path) / 'grainstats_dms.npy'}"
        )
        log(logger, msg)
        # Compute Metrics
        coefficient_of_uniformity, coefficient_of_gradation, D10, D30, D60 = (
            coefficients_of_uniformity_and_gradation(
                dms, plot_dir_path=output_dir_path,
                normalize_by_mass=normalize_by_mass)
        )
        F, n_branches = calculate_fabric_tensor(
            xms,dms, branch_vector_method=branch_vector_method)
        # Write results
        with open(output_file_path, 'a') as f:
            f.write(f"{filename},{slice_i[0]},{slice_i[1]},"
                    f"{F[0,0]},{F[1,1]},{F[2,2]},{F[1,2]},{F[0,2]},{F[0,1]},"
                    f"{coefficient_of_uniformity},{coefficient_of_gradation},"
                    f"{n},{n_branches},"
                    f"{D10},{D30},{D60}\n")

def branch_vectors_to_fabric(
    branch_vectors: list,
    detail_print: bool = False
) -> np.ndarray:
    # Reshape the branch vectors into a numpy object, by flattening the list
    # along the python list dimension
    p = 0
    for i in range(len(branch_vectors)):
        p += len(branch_vectors[i])
    if detail_print:
        print(f'Found {p} individual vectors')
    branch_vectors_np = np.zeros((p,3))
    p = 0
    for i in range(len(branch_vectors)):
        p_i = len(branch_vectors[i])
        branch_vectors_np[p:p+p_i,:] = branch_vectors[i]
        p += p_i
    # Add the negative of all the vectors
    branch_vectors_np = np.concatenate((branch_vectors_np, -branch_vectors_np), axis=0)
    if detail_print:
        print('Adding negatives')
        print(branch_vectors_np.shape)
    # Remove duplicate vectors
    branch_vectors_np = np.unique(branch_vectors_np, axis=0)
    if detail_print:
        print('Removing duplicates')
        print(branch_vectors_np.shape)
    cutoff = 0.0
    if detail_print:
        # Plot the vectors length distribution
        plt.figure()
        plt.hist(np.linalg.norm(branch_vectors_np, axis=1), bins=1000)
        yl = plt.ylim()[1]
        plt.plot([cutoff, cutoff], [0, yl], 'r')
        plt.show()
    # Remove vectors that are too short
    branch_vectors_np = branch_vectors_np[np.linalg.norm(branch_vectors_np, axis=1) > cutoff]
    if detail_print:
        print('Removing short vectors')
        print(branch_vectors_np.shape)
    # Normalize the vectors
    branch_vectors_np = branch_vectors_np / np.linalg.norm(branch_vectors_np, axis=1).reshape(-1,1)
    if detail_print:
        print('Normalizing')
        print(branch_vectors_np.shape)
    # Compute the fabric tensor
    fabric_tensor = np.zeros((3,3))
    for i in range(3):
        for j in range(3):
            fabric_tensor[i,j] = np.mean(
                branch_vectors_np[:,i]*branch_vectors_np[:,j]
            )
    if detail_print:
        print('Fabric Tensor')
        print(fabric_tensor)
    return fabric_tensor


def calculate_ellipse_essentials(data, logger=None):
    """Calculate the center and diameters of the ellipsoids.
    ----------
    Parameters
    ----------
    data : np.ndarray
        Array containing data extracted from grainstats.csv
    logger : None or logging.Logger, optional
        Logger object if using logging module to save a log and print to
        console. Only prints to console if None. Defaults to None.
    -------
    Returns
    -------
    list
        List of two arrays, xms and dms. Related to size/position of ellipsoids.
    """
    n = data.shape[0]
    ds = np.reshape(data[:,0:3], (n,3))
    Vs = np.reshape(data[:,3:12], (n,3,3))
    bs = np.reshape(data[:,12:15], (n,3))
    xms = np.zeros((n,3))
    dms = np.zeros((n,3))
    # Iterate through data extracted from grainstats.csv
    for ind in range(n):
        V = Vs[ind,:,:]
        b = bs[ind,:].reshape(3,1)
        D = np.diag(ds[ind,:])
        Di = np.diag(1/ds[ind,:])
        A = np.matmul(V, np.matmul(D, V.T))
        # Choose the farthest "corners" of the ellipsoid, and average to get
        # the center. Because the corners are both negative and positive
        # extents, this is also the diameter rather than the radius.
        for dim in range(3):
            RHS = np.zeros((3,2))
            RHS[dim,0] = 1
            RHS[dim,1] = -1
            RHS = RHS
            x = np.linalg.solve(A, RHS-b)
            # Compute the distance between the two points in x
            d = np.linalg.norm(x[:,0]-x[:,1])
            dms[ind,dim] = d
            if dim == 0:
                xm = np.mean(x, axis=1)
                xms[ind,:] = xm
        # Alternative method for the sizes: (not sure which to trust more,
        # add options and do both.)
        # dms2[ind,:] = 2*np.diag(Di)
    # Sort dms so that index 0 is the largest diameter
    dms = np.sort(dms, axis=1)[:,::-1]
    return xms, dms

def calculate_fabric_tensor(
    xms,
    dms,
    branch_vector_method='2-radius',
    logger=None,
):
    # Number of particles
    n = xms.shape[0]
    # Now Fabric Tensor. See Notebook 29
    dm = dms[:,0]
    # Calculate the equivalent radius of the particles,
    # V = pi/6 *A*B*C (principal diameters).
    vol = np.pi/6 * np.product(dms,axis=1)
    R = (3*vol/(4*np.pi))**(1/3)
    tree = spatial.KDTree(xms)
    # We don't know what the length of this will be.
    branch_vectors = []
    # Nearest neighbor methods
    if branch_vector_method == 'ball' or branch_vector_method == 'knn':
        for ind in range(n):
            center = xms[ind,:]
            # Radius to be used for nearest neighbors search
            # dm is actually the diameter along the major axis, so assuming
            # that the other particle has the same size, this is the test
            # distance between them.
            if branch_vector_method == 'ball':
                radius = dm[ind]
                # Find the nearest neighbors
                ind2 = tree.query_ball_point(center, radius)
            elif branch_vector_method == 'knn':
                # Find the 2 nearest neighbors
                ind2 = tree.query(center, k=2)[1]
            # # #
            # Remove the center point
            if type(ind2) is np.ndarray:
                ind2 = np.delete(ind2, np.where(ind2==ind))
            else:
                ind2.remove(ind)
            # # #
            if len(ind2)>0:
                # Find the vectors from the center to the neighbors
                branch_vectors.append(xms[ind2,:] - center)
    elif branch_vector_method == '2-radius':
        # This method first finds the sets particles that are within some
        # "max_distance" of each other. Then, we loop over all these pairs and
        # compare their actual distances to their (bounding ellipsoid)
        # equivalent radii. Other methods may use the actual STL volume
        # equivalent radii, but this script has no access to the STLs directly.
        max_distance = np.linalg.norm(np.max(xms) - np.min(xms))

        # If particles are all the same size, dividing by n would give the
        # distance between the particles. But if some particles are smaller,
        # we would underestimate the distance. Thus, divide by a smaller number
        # to give an upper bound.
        max_distance /= np.sqrt(n)
        # Also omit the sqrt(3) factor, to give an upper bound.
        # max_distance /= np.sqrt(3)
        # For safety, make this bound even bigger. This is the maximum
        # searchable/query distance between any two particles. Decrease this
        # to increase speed, but increase the chance of missing some pairs.
        max_distance *= 10
        log(logger, f'Max query distance: {max_distance}')
        A = tree.sparse_distance_matrix(
            tree, max_distance=max_distance, output_type = 'ndarray')
        # # #
        print(
            logger,
            'Finished constructing sparse distance matrix,'
            ' starting check against radii')
        branch_vectors = []
        n_nonzeros = A.shape[0]
        for i in range(n_nonzeros):
            row = A[i]
            # Minimum size check
            if row[2] > 1e-8:
                # Maximum size check based on the equivalent radii of the two
                # involved spheres
                radius_max_distance = R[row[0]] + R[row[1]]
                if row[2] <= radius_max_distance:
                    branch_vectors.append(xms[row[1],:] - xms[row[0],:])
    log(logger, f'Number of particles: {n}')
    log(logger, f'Number of branch vectors: {len(branch_vectors)}')
    fabric_tensor = branch_vectors_to_fabric(branch_vectors)
    log(logger, f'Fabric Tensor: {fabric_tensor}')
    n_branches = len(branch_vectors)
    return fabric_tensor, n_branches


def coefficients_of_uniformity_and_gradation(
    dms,
    plot_dir_path=None,
    normalize_by_mass=False,
    logger=None,
):
    # See notebook 27 for more details on similar approach
    # as the particle must pass through a square / circular mesh,
    # not a 1D thing
    min_dim = dms[:,1]
    # Normalize by mass
    if normalize_by_mass:
        volume = np.product(dms, axis=1)
        print(f'{np.product(dms, axis=1)=}')
        volume = volume / np.max(volume)
        # Janky, but doesn't matter.
        n_repeats = np.round(volume * 100).astype(int)
        min_dim = np.repeat(min_dim, n_repeats)
    # Get percentiles 10, 30, and 60
    percentiles = np.array([10,30,60])
    [D10,D30,D60] = np.percentile(min_dim, percentiles)
    # Make plot
    if plot_dir_path is not None:
        # Make histogram
        # plot the distribution function
        # plt.figure()
        # plt.subplot(1,2,1)
        fig, axes = plt.subplots(
            nrows=1, ncols=2, dpi=300, facecolor='white',
            constrained_layout=True)
        axes[0].hist(min_dim, bins=1000, density=True)
        axes[0].set_xlabel('Minimum dimension of bounding ellipsoid')
        axes[0].set_ylabel('Relative frequency')
        axes[0].set_title('Probability Distribution')
        # plot the cumulative distribution function
        axes[1].hist(min_dim, bins=1000, density=True, cumulative=True)
        axes[1].set_xlabel('Minimum dimension of bounding ellipsoid')
        axes[1].set_ylabel('Cumulative relative frequency')
        # plot percentiles
        axes[1].plot([D10,D30,D60], percentiles/100, 'ro')
        # axes[1].set_xscale('log')
        axes[1].set_title('Grading Curve')
        fig.savefig(str(Path(plot_dir_path) / 'grain_size_distribution.png'))
    # Calculate coefficients
    coefficient_of_uniformity = D60/D10
    coefficient_of_gradation = D30*D30/(D60*D10)
    log(logger, f'Coefficient of uniformity: {coefficient_of_uniformity:.3f}')
    log(logger, f'Coefficient of gradation: {coefficient_of_gradation:.3f}')
    return coefficient_of_uniformity, coefficient_of_gradation, D10, D30, D60

def generate_stats(
    in_dir_path,
    out_dir_path,
    verbose=0,
    tolerance=1e-5,
    max_iters=50000,
    logger=None,
):
    """Generate grainstats.csv file to be analyzed by analyze_grainstats()

    Parameters
    ----------
    in_dir_path : str or pathlib.Path
        Path to directory containing STL files from which ellipsoids are
        generated.
    out_dir_path : str or pathlib.Path
        Path to directory where grainstats.csv will be saved.
    verbosity : int, optional
        Level of verbosity output from optimization, by default 0
    tolerance : float, optional
        Tolerance for ellipsoid optimization, by default 1e-5
    max_iters : int, optional
        Max iterations of ellipsoid optimization, by default 50000
    logger : logging.Logger, optional
        If not None, print statements will also be passed to a file determined
        at the creation of the Logger.
        See segmentflow.workflows.Workflow.create_logger. Defaults to None.
    """
    # Set up the output directory in the same directory on level
    # above the stl files.
    # Note that the stl files will be APPENDED to the csv file, if it exists
    output_file = Path(out_dir_path) / 'grainstats.csv'
    # stl_dir = os.path.join(segmentflow_dir,'stls')
    stl_dir = Path(in_dir_path)
    log(logger, f'STL Directory: {stl_dir}')
    # Get the list of stl files
    stl_files = [p for p in stl_dir.glob('*.stl')]
    n_particles = len(stl_files)
    log(logger, f'{n_particles} STL files found.')
    #---------------------------------#
    # For testing and debugging only: #
    #---------------------------------#
    # stl_files = stl_files[0:50]
    update_interval = int(n_particles//100)
    for i,file in enumerate(stl_files):
        m = mesh.Mesh.from_file(file)
        vertices = m.vectors.reshape((-1, 3))
        [D_sorted, V_sorted, b_val, violation] = get_min_ellipsoid_for_points(
            vertices, tol=tolerance, max_iters=max_iters
        )
        # potentially print results, based on verbose level
        if verbose >= 2 or (
            verbose >= 1 and (
                update_interval == 0
                or i % update_interval == 0
                or i == n_particles-1
            )
        ):
            prefix = f'File {i+1}/{n_particles}: {file}'
            msg = (
                f'{prefix}; Diag entries:'
                f' {D_sorted[0]:.3f},{D_sorted[1]:.3f}, {D_sorted[2]:.3f};'
                f' Max Violation {violation:.2e}'
            )
            log(logger, msg)
        # Get ID by splitting the filename (no suffix) and choosing the
        # piece after the last '_' (converts entire stem if no '_')
        id = file.stem.split('_')[-1]
        # Try converting to an integer
        try:
            id = int(id)
        except ValueError:
            pass
        # Write to file
        write_row_to_file(output_file, id, D_sorted, V_sorted, b_val, violation)

def generate_stats_mpi(
    stl_path_list,
    rank,
    out_dir_path,
    verbosity=0,
    tolerance=1e-5,
    max_iters=50000,
    logger=None,
):
    """Generate grainstats.csv file (to be analyzed by analyze_grainstats)
    with multiple processes using mpi4py.

    Parameters
    ----------
    stl_path_list : list
        Path to directory containing STL files of which statistics will be
        generated.
    rank : int
        Process number for returned by mpi4py.MPI.COMM_WORLD.get_rank
    out_dir_path : str or pathlib.Path
        Path to directory where grainstats.csv will be saved.
    verbosity : int, optional
        Level of verbosity output from optimization, by default 0
    tolerance : float, optional
        Tolerance for ellipsoid optimization, by default 1e-5
    max_iters : int, optional
        Max iterations of ellipsoid optimization, by default 50000
    logger : logging.Logger, optional
        If not None, print statements will also be passed to a file determined
        at the creation of the Logger.
        See segmentflow.workflows.Workflow.create_logger. Defaults to None.
    """
    # Set up the output directory in the same directory on level
    # above the stl files.
    # Note that the stl files will be APPENDED to the csv file, if it exists
    output_file = Path(out_dir_path) / f'grainstats_{rank}.csv'
    # Get the list of stl files
    n_particles = len(stl_path_list)
    update_interval = int(n_particles//100)
    for i, file in enumerate(stl_path_list):
        m = mesh.Mesh.from_file(file)
        vertices = m.vectors.reshape((-1, 3))
        [D_sorted, V_sorted, b_val, violation] = get_min_ellipsoid_for_points(
            vertices,tol=tolerance,max_iters=max_iters)
        # potentially print results, based on verbose level
        if verbosity >= 2 or (
            verbosity >= 1 and (
                update_interval == 0
                or i % update_interval == 0
                or i == n_particles-1
            )
        ):
            msg = (
                f'Process {rank}: File {i+1}/{n_particles};'
                f' Diag entries:'
                f' {D_sorted[0]:.3f},{D_sorted[1]:.3f}, {D_sorted[2]:.3f};'
                f' Max Violation: {violation:.2e}'
            )
            log(logger, msg)
        # Get ID by splitting the filename (no suffix) and choosing the
        # piece after the last '_' (converts entire stem if no '_')
        id = file.stem.split('_')[-1]
        # Try converting to an integer
        try:
            id = int(id)
        except ValueError:
            pass
        # Write to file
        write_row_to_file(output_file, id, D_sorted, V_sorted, b_val, violation)

def get_min_ellipsoid_for_points(vertices, tol=1e-5, max_iters=25000):
    """Compute the minimum volume (Lowner-John) ellipsoid containing all
    vertices. This uses cvxpy and may have some tolerance for points outside
    of the ellipsoid. The max violation should be checked, before using the
    results (constraints are on the scale of 1).
    The ellipse is given by {x : || A@x + b ||_2 <= 1} (A = V D V^T)
    See here for a 2D example:
    https://notebook.community/stephenbeckr/convex-optimization-class/CVX_demo/cvxpy_intro

    Inputs:
        vertices: (n,3) array of vertices or points
        tol: tolerance for cvxpy solver
        max_iters: maximum number of iterations for cvxpy solver

    Outputs:
        D_sorted: (3,) eigenvalues of the inverse map for PSD matrix A, NOT THE
            SEMI-AXES LENGTHS
        V_sorted: (3,3) eigenvectors of the inverse map for PSD matrix A, NOT
            THE SEMI-AXES DIRECTIONS
        b_val: (3,1) vector for the offset
    """

    # Remove duplicate rows, important for segmentflow stl files
    vertices = np.unique(vertices, axis=0)

    n = vertices.shape[0]

    A = cvx.Variable((3, 3), PSD=True)
    b = cvx.Variable((3,1))
    obj = cvx.Maximize(cvx.log_det(A))
    constraints = [ cvx.norm(A@vertices[i:i+1,:].T + b) <= 1 for i in
                    range(n) ]
    prob = cvx.Problem(obj, constraints)
    # Defaults: max_iters=2500, eps=1e-4
    prob.solve(solver='SCS', verbose=False, max_iters=max_iters, eps=tol)

    # Compute constraint violation using final value of A and b:
    violation = [np.linalg.norm(A.value@vertices[i:i+1,:].T + b.value) - 1
                    for i in range(n)]

    # Decomposition
    D,V = np.linalg.eig(A.value)

    # sort by largest D
    inds = np.argsort(D)[::-1]
    D_sorted = D[inds]
    V_sorted = V[:,inds]

    return D_sorted, V_sorted, b.value, np.max(violation)

def log(logger, msg):
    if logger is None:
        print(msg)
    else:
        logger.info(msg)

def write_row_to_file(output_file, id, D_sorted, V_sorted, b_val, violation):
    """ For both LAMMPS and Segmentflow, write a row to the output file """
    with open(output_file, 'a') as f:
        f.write(f'{id},{D_sorted[0]},{D_sorted[1]},{D_sorted[2]}')
        for i in range(3):
            for j in range(3):
                f.write(f',{V_sorted[i,j]}')
        for i in range(3):
            f.write(f',{b_val[i,0]}')
        f.write(f',{violation}\n')
