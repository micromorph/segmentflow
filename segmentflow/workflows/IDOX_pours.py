import matplotlib.pyplot as plt
from pathlib import Path
from segmentflow import segment, view, mesh
from segmentflow.workflows.workflow import Workflow
from skimage import filters
import sys


class IDOX_pours(Workflow):
    def __init__(self, yaml_path=None, args=None):
        # Initialize parent class to set yaml_path
        super().__init__(yaml_path=yaml_path, args=args)
        self.name = Path(__file__).stem
        self.description = (
            'This workflow segments IDOX particles and Kel-F binder in a CT scan'
            ' and outputs a labeled TIF stack and/or STL files corresponding'
            ' to each segmented particle.'
            ' Developed for v0.0.1, updated for v0.0.5.'
        )
        self.categorized_input_shorthands = {
            'A. Files' : {
                'in_dir_path'  : '01. Input dir path',
                'file_suffix'  : '02. File suffix',
                'slice_crop'   : '03. Slice crop',
                'row_crop'     : '04. Row crop',
                'col_crop'     : '05. Column crop',
                'spatial_res'  : '06. Pixel size',
                'out_dir_path' : '07. Path to save output dir',
                'out_prefix'   : '08. Output prefix',
                'overwrite'    : '09. Overwrite files'
            },
            'B. Preprocess' : {
                'pre_seg_med_filter' : '01. Apply median filter',
                'rescale_range'      : '02. Rescale intensity range',
            },
            'C. Segmentation' : {
                'hist_nbins' : '01. Histogram bins for calculating thresholds',
                'hist_smooth_size' :
                    '02. Size of smoothing kernel for histogram peak ID',
                'thresh_hist_ylims' :
                    '03. Upper and lower y-limits of histogram',
                'fill_holes'        : '04. Fill holes in semantic seg',
                'semantic_med_filt' : '05. Reduce noise in semantic seg',
                'perform_seg'       : '06. Perform instance seg',
                'min_peak_dist'     : '07. Min peak distance',
                'exclude_borders'   : '08. Exclude border particles',
            },
            'D. Output' : {
                'save_voxels'          : '01. Save labeled voxels',
                'nslices'              : '02. Number of slices in checkpoint plots',
                'slices'               : '03. Specific slices to plot',
                'save_stls'            : '04. Create STL files',
                'suppress_save_msg'    : '05. Suppress save message for each STL file',
                'n_erosions'           : '06. Number of pre-surface meshing erosions',
                'instance_med_filt'    : '07. Smooth voxels with median filtering',
                'step_size'            : '08. Marching cubes voxel step size',
                'mesh_smooth_n_iters'  : '09. Number of smoothing iterations',
                'mesh_simplify_n_tris' : '10. Target number of triangles/faces',
                'mesh_simplify_factor' : '11. Simplification factor per iteration',
            },
        }

        self.default_values = {
            'in_dir_path'          : 'REQUIRED',
            'file_suffix'          : 'tiff',
            'slice_crop'           : None,
            'row_crop'             : None,
            'col_crop'             : None,
            'out_dir_path'         : 'REQUIRED',
            'out_prefix'           : '',
            'overwrite'            : False,
            'pre_seg_med_filter'   : False,
            'rescale_range'        : None,
            'hist_nbins'           : 256,
            'hist_smooth_size'     : 3,
            'thresh_hist_ylims'    : [0, 20000000],
            'fill_holes'           : True,
            'semantic_med_filt'    : True,
            'perform_seg'          : True,
            'min_peak_dist'        : 6,
            'exclude_borders'      : True,
            'save_voxels'          : True,
            'nslices'              : 3,
            'slices'               : None,
            'save_stls'            : True,
            'n_erosions'           : 1,
            'suppress_save_msg'    : True,
            'instance_med_filt'    : True,
            'spatial_res'          : 1,
            'step_size'            : 1,
            'mesh_smooth_n_iters'  : None,
            'mesh_simplify_n_tris' : None,
            'mesh_simplify_factor' : None,
        }

    def run(self):
        show_checkpoints = False
        checkpoint_save_dir = self.ui['out_dir_path']

        #-------------#
        # Load images #
        #-------------#
        print()
        imgs = segment.load_images(
            self.ui['in_dir_path'],
            slice_crop=self.ui['slice_crop'],
            row_crop=self.ui['row_crop'],
            col_crop=self.ui['col_crop'],
            convert_to_float=True,
            file_suffix=self.ui['file_suffix'],
            logger=self.logger
        )
        # Generate raw imgs viz
        fig, axes = view.vol_slices(
            imgs,
            slices=self.ui['slices'],
            nslices=self.ui['nslices'],
            print_slices=False,
            fig_w=7.5,
            dpi=300
        )
        fig_n = 0
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='raw-imgs'
        )

        #-------------------#
        # Preprocess images #
        #-------------------#
        # Plot intensity rescale histogram
        imgs_med = segment.preprocess(
            imgs,
            median_filter=self.ui['pre_seg_med_filter'],
            logger=self.logger
        )
        fig, ax = view.histogram(
            imgs_med,
            mark_percentiles=self.ui['rescale_range'],
            logger=self.logger
        )
        fig_n += 1
        segment.output_checkpoints(
            fig, show=show_checkpoints, save_path=checkpoint_save_dir,
            fn_n=fig_n, fn_suffix='intensity-rescale-hist')
        # Preprocess images
        imgs_pre = segment.preprocess(
            imgs,
            median_filter=False,
            rescale_intensity_range=self.ui['rescale_range'],
            logger=self.logger
        )
        # Generate preprocessed viz
        fig, axes = view.vol_slices(
            imgs_pre,
            slices=self.ui['slices'],
            nslices=self.ui['nslices'],
            print_slices=False,
            fig_w=7.5,
            dpi=300
        )
        fig_n += 1
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='preprocessed-imgs'
        )

        #-----------------------#
        # Semantic segmentation #
        #-----------------------#
        # Calc semantic seg threshold values and generate histogram
        thresholds, fig, ax = segment.threshold_multi_min(
            imgs_pre,
            nbins=self.ui['hist_nbins'],
            hist_smooth_size=self.ui['hist_smooth_size'],
            return_fig_ax=True,
            ylims=self.ui['thresh_hist_ylims']
        )
        fig_n += 1
        segment.output_checkpoints(
            fig, show=show_checkpoints, save_path=checkpoint_save_dir,
            fn_n=fig_n, fn_suffix='semantic-seg-hist')
        # Segment images with threshold values
        imgs_semantic = segment.isolate_classes(imgs_pre, thresholds)
        # Calc particle to binder ratio (voxels)
        particles_to_binder = segment.calc_voxel_stats(
            imgs_semantic, logger=self.logger
        )
        # Generate semantic label viz
        fig, axes = view.vol_slices(
            imgs_semantic,
            slices=self.ui['slices'],
            nslices=self.ui['nslices'],
            print_slices=False,
            fig_w=7.5,
            dpi=300
        )
        fig_n += 1
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='semantic-seg-imgs'
        )

        #------------------#
        # Fill small holes #
        #------------------#
        if self.ui['fill_holes']:
            imgs_semantic = segment.fill_holes(
                imgs_semantic, logger=self.logger
            )
            # Calc particle to binder ratio (voxels)
            particles_to_binder = segment.calc_voxel_stats(
                imgs_semantic, logger=self.logger
            )
            # Generate semantic label viz
            fig, axes = view.vol_slices(
                imgs_semantic,
                slices=self.ui['slices'],
                nslices=self.ui['nslices'],
                print_slices=False,
                fig_w=7.5,
                dpi=300
            )
            fig_n += 1
            segment.output_checkpoints(
                fig,
                show=show_checkpoints,
                save_path=checkpoint_save_dir,
                fn_n=fig_n,
                fn_suffix='semantic-seg-imgs-holes-filled'
            )

        #---------------------------#
        # Smooth with Median filter #
        #---------------------------#
        if self.ui['semantic_med_filt']:
            imgs_semantic = filters.median(imgs_semantic)
            # Calc particle to binder ratio (voxels)
            particles_to_binder = segment.calc_voxel_stats(
                imgs_semantic, logger=self.logger
            )
            # Generate semantic label viz
            fig, axes = view.vol_slices(
                imgs_semantic,
                slices=self.ui['slices'],
                nslices=self.ui['nslices'],
                print_slices=False,
                fig_w=7.5,
                dpi=300
            )
            fig_n += 1
            segment.output_checkpoints(
                fig,
                show=show_checkpoints,
                save_path=checkpoint_save_dir,
                fn_n=fig_n,
                fn_suffix='semantic-seg-imgs-median-filtered'
            )

        #-----------------------#
        # Instance segmentation #
        #-----------------------#
        if self.ui['perform_seg']:
            print()
            # Clear up memory
            imgs = None
            imgs_med = None
            imgs_pre = None
            imgs_instance = segment.watershed_segment(
                imgs_semantic==2,
                min_peak_distance=self.ui['min_peak_dist'],
                exclude_borders=self.ui['exclude_borders'],
                return_dict=False,
                logger=self.logger
            )
            # Generate instance label viz
            fig, axes = view.color_labels(
                imgs_instance,
                slices=self.ui['slices'],
                nslices=self.ui['nslices'],
                fig_w=7.5,
                dpi=300
            )
            fig_n += 1
            segment.output_checkpoints(
                fig, show=show_checkpoints, save_path=checkpoint_save_dir,
                fn_n=fig_n, fn_suffix='instance-seg-imgs')
            # Merge semantic and instance segs to represent binder and particles
            imgs_labeled = segment.merge_segmentations(
                imgs_semantic, imgs_instance
            )

        #-------------#
        # Save voxels #
        #-------------#
        if self.ui['save_voxels']:
            if self.ui['perform_seg']:
                segment.save_images(
                    imgs_labeled,
                    Path(self.ui['out_dir_path']) / f"{self.ui['out_prefix']}_labeled_voxels"
                )
            else:
                segment.save_images(
                    imgs_semantic,
                    Path(self.ui['out_dir_path']) / f"{self.ui['out_prefix']}_semantic_voxels"
                )

        #----------------------------------------#
        # Create Surface Meshes of Each Particle #
        #----------------------------------------#
        if self.ui['perform_seg'] and self.ui['save_stls']:
            print()
            segment.save_as_stl_files(
                imgs_labeled,
                self.ui['out_dir_path'],
                self.ui['out_prefix'],
                suppress_save_msg=self.ui['suppress_save_msg'],
                slice_crop=self.ui['slice_crop'],
                row_crop=self.ui['row_crop'],
                col_crop=self.ui['col_crop'],
                stl_overwrite=self.ui['overwrite'],
                spatial_res=self.ui['spatial_res'],
                n_erosions=self.ui['n_erosions'],
                median_filter_voxels=self.ui['instance_med_filt'],
                voxel_step_size=self.ui['step_size'],
                region_ids_to_skip=1,  # DO not mesh binder region (id = 1)
                logger=self.logger
            )

            #----------------------------------------------#
            # Postprocess surface meshes for each particle #
            #----------------------------------------------#
            if (
                self.ui['mesh_smooth_n_iters'] is not None
                or self.ui['mesh_simplify_n_tris'] is not None
                or self.ui['mesh_simplify_factor'] is not None
            ):
                print()
                # Iterate through each STL file, load the mesh, and smooth/simplify
                mesh.postprocess_meshes(
                    self.ui['stl_dir_location'],
                    smooth_iter=self.ui['mesh_smooth_n_iters'],
                    simplify_n_tris=self.ui['mesh_simplify_n_tris'],
                    iterative_simplify_factor=self.ui['mesh_simplify_factor'],
                    recursive_simplify=False,
                    resave_mesh=True,
                    logger=self.logger
                )


if __name__ == '__main__':
    print()
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print('Welcome to Segmentflow!')
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print()
    # Pass path to YAML from args and store in workflow class
    workflow = IDOX_pours(args=sys.argv[1:])
    # Load input data from YAML and store as UI attribute
    workflow.read_yaml()
    # Create and store a logger object as an attribute for saving a log file
    workflow.create_logger()
    try:
        workflow.run()
    except Exception as error:
        workflow.logger.exception(error)
    print()
    print('~~~~~~~~~~~~~~~~~~~~~')
    print('Successful Completion')
    print('~~~~~~~~~~~~~~~~~~~~~')
    print()

