from pathlib import Path
import pandas as pd
from segmentflow import segment, view, grainstattools
from segmentflow.workflows.workflow_mpi import Workflow_MPI
import sys


class Grainstats_MPI(Workflow_MPI):
    def __init__(self, yaml_path=None, args=None):
        # Initialize parent class to set yaml_path
        super().__init__(yaml_path=yaml_path, args=args)
        self.name = Path(__file__).stem
        self.description = (
            "This workflow is based on Grant Norman's GrainStatTools code for"
            " assessing the size distribution of particles by fitting minimum"
            " bounding ellipsoids to points extracted from STL files."
            " This is similar to `workflows.grainstats`, although uses the"
            " package `mpi4py` to run in parallel."
            " Developed for Segmentflow v0.0.5."
        )
        self.categorized_input_shorthands = {
            'A. Input' : {
                'in_dir_path' : '01. Input dir path to STL files',
            },
            'B. Output' : {
                'out_dir_path' : '01. Output dir path',
                'out_prefix'   : '02. Output prefix & dir name',
            },
        }
        self.default_values = {
            'in_dir_path'  : 'REQUIRED',
            'out_dir_path' : 'REQUIRED',
            'out_prefix'   : '',
        }

    def run(self):
        """Carry out workflow (self.name) as described by self.description.
        """
        self.logger.info(f'Beginning workflow: {self.name}')
        show_checkpoints = False
        checkpoint_save_dir = self.ui['out_dir_path']
        # For CVXPY solver. Impacts speed.
        tol = 1e-5
        max_iters = 50000
        # Set level of verbosity
        # --> 0: minimal updates
        # --> 1: 100 updates per segmentflow folder
        # --> 2: 1 update per stl file (~5000 per folder or LAMMPs dump file)
        verbosity = 2
        # Get the list of stl files
        stl_dir_path = Path(self.ui['in_dir_path'])
        self.logger.info(f'Retrieving STLs: {stl_dir_path}')
        # This line takes only the first 10 STL files. Remove & uncomment line
        # below to process all STLs.
        stl_path_list = [p for p in stl_dir_path.glob('*.stl')][:10]
        # stl_path_list = [p for p in stl_dir_path.glob('*.stl')]
        files_per_process = len(stl_path_list) // self.size
        # Total number of files
        n_files = len(stl_path_list)
        # Calculate the number of files per process
        files_per_process = n_files // self.size
        # Calculate the start and end indices for each process
        start_idx = self.rank * files_per_process
        # Ensure the last process takes the remaining files
        if self.rank == self.size - 1:
            end_idx = n_files
        else:
            end_idx = start_idx + files_per_process
        # Iterate through files in chunk determined by rank
        data = []
        stl_path_sublist = [stl_path_list[i] for i in range(start_idx, end_idx)]
        # Run file generation functions from GranStatTools in parallel
        msg = (
            f'Process {self.rank}:'
            f' Generating minimum bounding ellipsoids for STLs'
            f' {start_idx} to {end_idx}...'
        )
        self.logger.info(msg)
        grainstattools.generate_stats_mpi(
            stl_path_sublist,
            self.rank,
            self.ui['out_dir_path'],
            verbosity=verbosity,
            tolerance=tol,
            max_iters=max_iters,
            logger=self.logger
        )
        # Wait for all processes to reach this point
        self.comm.Barrier()
        # Combine all CSVs into a single DataFrame in the first process
        if self.rank == 0:
            df_list = []
            for i in range(self.size):
                csv_path = (
                    Path(self.ui['out_dir_path']) / f'grainstats_{i}.csv'
                )
                df = pd.read_csv(csv_path, header=None)
                df_list.append(df)
                # csv_path.unlink()
            df_all = pd.concat(df_list, ignore_index=True)
            # main_csv_path = Path(f'output.csv')
            main_csv_path = Path(self.ui['out_dir_path']) / 'grainstats.csv'
            df_all.to_csv(main_csv_path, index=False)
            print(f"Data from all processes written to {main_csv_path}")
            # Analyze file containing ellipsoid information, as produced by
            # grainstattools.generate_stats()
            self.logger.info('Analyzing minimum bounding ellipsoids...')
            grainstattools.analyze_stats(main_csv_path, logger=self.logger)


if __name__ == '__main__':
    # Pass path to YAML from args and store in workflow class
    workflow = Grainstats_MPI(args=sys.argv[1:])
    if workflow.rank == 0:
        print()
        print('~~~~~~~~~~~~~~~~~~~~~~~')
        print('Welcome to Segmentflow!')
        print('~~~~~~~~~~~~~~~~~~~~~~~')
        print()
        print(f'Running on {workflow.size} processors...')
    # Load input data from YAML and store as UI attribute
    workflow.read_yaml()
    # Only make output directory if first process
    if workflow.rank == 0:
        workflow.make_output_dir()
    # Wait for all processes to reach this point
    workflow.comm.Barrier()
    # Create and store a logger object as an attribute for saving a log file
    workflow.create_logger()

    try:
        workflow.run()
    except Exception as error:
        workflow.logger.exception(error)

    # Wait for all processes to reach this point
    workflow.comm.Barrier()

    if workflow.rank == 0:
        print()
        print('~~~~~~~~~~~~~~~~~~~~~')
        print('Successful Completion')
        print('~~~~~~~~~~~~~~~~~~~~~')
        print()
