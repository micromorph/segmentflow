import matplotlib.pyplot as plt
from pathlib import Path
from segmentflow import segment, view, mesh
from segmentflow.workflows.workflow import Workflow
from skimage import filters
import sys


class Test_parameters(Workflow):
    def __init__(self, yaml_path=None, args=None):
        # Initialize parent class to set yaml_path
        super().__init__(yaml_path=yaml_path, args=args)
        self.name = Path(__file__).stem
        self.description = (
            'This workflow is for testing parameters for preprocessing and'
            ' semantic segmentation. This is a good starting point before'
            ' running another workflow to determine which parameters to use!'
            ' Developed for v0.0.5.'
        )
        self.categorized_input_shorthands = {
            'A. Input' : {
                'in_dir_path'  : '01. Input dir path',
                'file_suffix'  : '02. File suffix',
                'slice_crop'   : '03. Slice crop',
                'row_crop'     : '04. Row crop',
                'col_crop'     : '05. Column crop',
            },
            'B. Preprocessing' : {
                'pre_seg_rad_filter' : '01. Apply radial filter',
                'pre_seg_med_filter' : '02. Apply median filter',
                'rescale_range'      : '03. Rescale intensity range',
            },
            'C. Segmentation' : {
                'hist_nbins' :
                    '01. Histogram bins for calculating auto thresholds',
                'hist_smooth_size' :
                    '02. Size of smoothing kernel for histogram peak ID',
                'thresh_hist_ylims' :
                    '03. Upper and lower y-limits of histogram',
                'manual_thresh' :
                    '04. Manual threshold values (if None, auto used)',
                'perform_sem_seg' :
                    '05. Perform semantic segmentation',
                'fill_holes' :
                    '06. Fill holes in semantic segmentation',
                'semantic_med_filt' :
                    '07. Reduce noise in semantic segmentation',
            },
            'D. Output' : {
                'out_dir_path' : '01. Path to save output dir',
                'out_prefix'   : '02. Output prefix & dir name',
                'overwrite'    : '03. Overwrite files',
                'nslices'      : '04. Checkpoint plots n slices',
                'slices'       : '05. Specific slices to plot',
            }
        }

        self.default_values = {
            'in_dir_path'          : 'REQUIRED',
            'file_suffix'          : 'tiff',
            'slice_crop'           : None,
            'row_crop'             : None,
            'col_crop'             : None,
            'out_dir_path'         : 'REQUIRED',
            'out_prefix'           : '',
            'overwrite'            : False,
            'pre_seg_rad_filter'   : False,
            'pre_seg_med_filter'   : False,
            'rescale_range'        : None,
            'hist_nbins'           : 256,
            'hist_smooth_size'     : 3,
            'thresh_hist_ylims'    : [0, 20000000],
            'manual_thresh'        : None,
            'perform_sem_seg'      : False,
            'fill_holes'           : True,
            'semantic_med_filt'    : True,
            'save_voxels'          : True,
            'nslices'              : 3,
            'slices'               : None,
        }

    def run(self):
        show_checkpoints = False
        checkpoint_save_dir = self.ui['out_dir_path']

        #-------------#
        # Load images #
        #-------------#
        imgs = segment.load_images(
            self.ui['in_dir_path'],
            slice_crop=self.ui['slice_crop'],
            row_crop=self.ui['row_crop'],
            col_crop=self.ui['col_crop'],
            convert_to_float=True,
            file_suffix=self.ui['file_suffix'],
            logger=self.logger
        )
        # Generate raw imgs viz
        fig, axes = view.vol_slices(
            imgs,
            slices=self.ui['slices'],
            nslices=self.ui['nslices'],
            print_slices=False,
            fig_w=7.5,
            dpi=300
        )
        fig_n = 0
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='raw-imgs'
        )

        #-------------------#
        # Preprocess images #
        #-------------------#
        # Apply radial filter to normalize beam hardening effect
        if self.ui['pre_seg_rad_filter']:
            imgs = segment.radial_filter(imgs, logger=self.logger)
            fig, axes = view.vol_slices(
                imgs,
                slices=self.ui['slices'],
                nslices=self.ui['nslices'],
                print_slices=False,
                fig_w=7.5,
                dpi=300
            )
            fig_n += 1
            segment.output_checkpoints(
                fig,
                show=show_checkpoints,
                save_path=checkpoint_save_dir,
                fn_n=fig_n,
                fn_suffix='radial-filtered'
            )

        # Plot histogram with range of rescale values
        imgs_med = segment.preprocess(
            imgs,
            median_filter=self.ui['pre_seg_med_filter'],
            logger=self.logger
        )
        fig, ax = view.histogram(
            imgs_med,
            mark_percentiles=self.ui['rescale_range'],
            logger=self.logger
        )
        fig_n += 1
        segment.output_checkpoints(
            fig, show=show_checkpoints, save_path=checkpoint_save_dir,
            fn_n=fig_n, fn_suffix='hist-intensity-rescale-vals')
        # Rescale intensity
        imgs_pre = segment.preprocess(
            imgs,
            median_filter=False,
            rescale_intensity_range=self.ui['rescale_range'],
            logger=self.logger
        )
        # Plot intensity rescaled histogram
        fig, ax = view.histogram(
            imgs_pre, logger=self.logger
        )
        fig_n += 1
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='hist-intensity-rescaled'
        )
        # Generate preprocessed viz
        fig, axes = view.vol_slices(
            imgs_pre,
            slices=self.ui['slices'],
            nslices=self.ui['nslices'],
            print_slices=False,
            fig_w=7.5,
            dpi=300
        )
        fig_n += 1
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='preprocessed-imgs'
        )

        #-------------------------------#
        # Semantic seg threshold values #
        #-------------------------------#
        # Calc semantic seg threshold values from histogram peaks
        thresholds, fig, ax = segment.threshold_multi_min(
            imgs_pre,
            nbins=self.ui['hist_nbins'],
            hist_smooth_size=self.ui['hist_smooth_size'],
            return_fig_ax=True,
            ylims=self.ui['thresh_hist_ylims'],
            logger=self.logger
        )
        fig_n += 1
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='hist-semantic-seg'
        )
        # Display manual thresholds
        if self.ui['manual_thresh'] is not None:
            thresh_vals = ', '.join([str(t) for t in self.ui['manual_thresh']])
            self.logger.info(
                f'Setting manual thresholds: {thresh_vals}'
            )
            thresholds = self.ui['manual_thresh']
            fig, ax = view.histogram_skimage(
                imgs_pre,
                mark_values=self.ui['manual_thresh'],
                ylims=self.ui['thresh_hist_ylims'],
                marker_label='Thresholds',
                logger=self.logger
            )
            fig_n += 1
            segment.output_checkpoints(
                fig,
                show=show_checkpoints,
                save_path=checkpoint_save_dir,
                fn_n=fig_n,
                fn_suffix='hist-manual-thresh'
            )

        #-----------------------#
        # Semantic segmentation #
        #-----------------------#
        if self.ui['perform_sem_seg']:
            # Segment images with threshold values
            imgs_semantic = segment.isolate_classes(imgs_pre, thresholds)
            # Calc particle to binder ratio (voxels)
            particles_to_binder = segment.calc_voxel_stats(
                imgs_semantic, logger=self.logger
            )
            # Generate semantic label viz
            fig, axes = view.vol_slices(
                imgs_semantic,
                slices=self.ui['slices'],
                nslices=self.ui['nslices'],
                print_slices=False,
                fig_w=7.5,
                dpi=300
            )
            fig_n += 1
            segment.output_checkpoints(
                fig,
                show=show_checkpoints,
                save_path=checkpoint_save_dir,
                fn_n=fig_n,
                fn_suffix='semantic-seg-imgs'
            )

            #------------------#
            # Fill small holes #
            #------------------#
            if self.ui['fill_holes']:
                imgs_semantic = segment.fill_holes(
                    imgs_semantic, logger=self.logger
                )
                # Calc particle to binder ratio (voxels)
                particles_to_binder = segment.calc_voxel_stats(
                    imgs_semantic, logger=self.logger
                )
                # Generate semantic label viz
                fig, axes = view.vol_slices(
                    imgs_semantic,
                    slices=self.ui['slices'],
                    nslices=self.ui['nslices'],
                    print_slices=False,
                    fig_w=7.5,
                    dpi=300
                )
                fig_n += 1
                segment.output_checkpoints(
                    fig,
                    show=show_checkpoints,
                    save_path=checkpoint_save_dir,
                    fn_n=fig_n,
                    fn_suffix='semantic-seg-imgs-holes-filled'
                )

            #---------------------------#
            # Smooth with Median filter #
            #---------------------------#
            if self.ui['semantic_med_filt']:
                imgs_semantic = filters.median(imgs_semantic)
                # Calc particle to binder ratio (voxels)
                particles_to_binder = segment.calc_voxel_stats(
                    imgs_semantic, logger=self.logger
                )
                # Generate semantic label viz
                fig, axes = view.vol_slices(
                    imgs_semantic,
                    slices=self.ui['slices'],
                    nslices=self.ui['nslices'],
                    print_slices=False,
                    fig_w=7.5,
                    dpi=300
                )
                fig_n += 1
                segment.output_checkpoints(
                    fig,
                    show=show_checkpoints,
                    save_path=checkpoint_save_dir,
                    fn_n=fig_n,
                    fn_suffix='semantic-seg-imgs-median-filtered'
                )


if __name__ == '__main__':
    print()
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print('Welcome to Segmentflow!')
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print()
    # Pass path to YAML from args and store in workflow class
    workflow = Test_parameters(args=sys.argv[1:])
    # Load input data from YAML and store as UI attribute
    workflow.read_yaml()
    # Create and store a logger object as an attribute for saving a log file
    workflow.create_logger()
    try:
        workflow.run()
    except Exception as error:
        workflow.logger.exception(error)
    print()
    print('~~~~~~~~~~~~~~~~~~~~~')
    print('Successful Completion')
    print('~~~~~~~~~~~~~~~~~~~~~')
    print()

