from pathlib import Path
from segmentflow import segment, view, grainstattools
from segmentflow.workflows.workflow import Workflow
import sys


class Grain_stats(Workflow):
    def __init__(self, yaml_path=None, args=None):
        self.name = Path(__file__).stem
        self.description = (
            "This workflow is based on Grant Norman's GrainStatTools code for"
            " assessing the size distribution of particles by fitting minimum"
            " bounding ellipsoids to points extracted from STL files."
            " Developed for Segmentflow v0.0.5."
        )
        self.categorized_input_shorthands = {
            'A. Input' : {
                'in_dir_path' : '01. Input dir path to STL files',
            },
            'B. Output' : {
                'out_dir_path' : '01. Output dir path',
                'out_prefix'   : '02. Output prefix',
                'overwrite'    : '03. Overwrite files',
            },
        }
        self.default_values = {
            'in_dir_path'  : 'REQUIRED',
            'out_dir_path' : 'REQUIRED',
            'out_prefix'   : '',
            'overwrite'    : False,
        }
        # Initialize parent class to set yaml_path
        super().__init__(yaml_path=yaml_path, args=args)

    def run(self):
        """Carry out workflow (self.name) as described by self.description.
        """
        show_checkpoints = False
        checkpoint_save_dir = self.ui['out_dir_path']
        self.logger.info(f'Beginning workflow: {self.name}')
        # logic for doing this in parallel, as it is pretty expensive.
        # it is mean to be run with an array job and called like:
        # python generate_min_ellipsoids.py $SLURM_ARRAY_TASK_ID $SLURM_ARRAY_TASK_MAX
        # if len(sys.argv) > 2 and '--' not in sys.argv[1]:
        #     print(sys.argv)
        #     run_id = int(sys.argv[1]) - 1
        #     max_id = int(sys.argv[2])
        # else:
        #     # for local runs, will run in series
        #     run_id = 0
        #     max_id = 1
        run_id = 0
        max_id = 1
        # For CVXPY solver. Impacts speed.
        tol = 1e-5
        max_iters = 50000
        # Set level of verbosity
        # --> 0: minimal updates
        # --> 1: 100 updates per segmentflow folder
        # --> 2: 1 update per stl file (~5000 per folder or LAMMPs dump file)
        verbose = 2
        # Run file generation functions from GranStatTools
        self.logger.info('Generating minimum bounding ellipsoids...')
        grainstattools.generate_stats(
            self.ui['in_dir_path'], self.ui['out_dir_path'], verbose=verbose,
            tol=tol, max_iters=max_iters, logger=self.logger)
        # Analyze file containing ellipsoid information, as produced by
        # granstattools.generate_stats()
        self.logger.info('Analyzing minimum bounding ellipsoids...')
        grainstattools.analyze_stats(
            Path(self.ui['out_dir_path']) / 'grain_stats.csv',
            logger=self.logger)


if __name__ == '__main__':
    print()
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print('Welcome to Segmentflow!')
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print()
    # Pass path to YAML from args and store in workflow class
    workflow = Grain_stats(args=sys.argv[1:])
    # Load input data from YAML and store as UI attribute
    workflow.read_yaml()
    # Create and store a logger object as an attribute for saving a log file
    workflow.create_logger()
    try:
        workflow.run()
    except Exception as error:
        workflow.logger.exception(error)
    print()
    print('~~~~~~~~~~~~~~~~~~~~~')
    print('Successful Completion')
    print('~~~~~~~~~~~~~~~~~~~~~')
    print()
