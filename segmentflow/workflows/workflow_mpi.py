import logging
import matplotlib.pyplot as plt
from mpi4py import MPI
from pathlib import Path
from segmentflow import segment, view
import sys
import yaml


class Workflow_MPI():
    def __init__(self, yaml_path=None, args=None):
        # Initialize MPI
        self.comm = MPI.COMM_WORLD
        self.size = self.comm.Get_size()
        self.rank = self.comm.Get_rank()
        # A Workflow object has to have some way of loading info/knowing what
        # to do, either with a yaml_path directly (used for testing) or args
        # (this is how a YAML file path is passed from the command line)
        self.yaml_path = None
        if yaml_path is None and args is None:
            raise ValueError(
                'Workflow must be intitialized with either yaml_path or args.')
        elif yaml_path is not None:
            self.yaml_path = Path(yaml_path).resolve()
        else:
            output = self.process_args(args)
            print(f'YAML path from args: {output}')
            self.yaml_path = Path(self.process_args(args)).resolve()

    def create_logger(self):
        # Set path for log file
        log_path = Path(self.ui['out_dir_path']) / 'segmentflow.log'
        # Create a logger object
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        # Create a formatter to define the log format
        formatter = logging.Formatter('%(asctime)s : %(message)s')
        # Create a file handler to write logs to a file
        file_handler = logging.FileHandler(str(log_path), 'w+')
        file_handler.setFormatter(formatter)
        # Create a stream handler to print logs to the console
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(formatter)
        # Add the handlers to the logger
        self.logger.addHandler(file_handler)
        self.logger.addHandler(console_handler)

    def make_output_dir(self):
        if not Path(self.ui['out_dir_path']).is_dir():
            print(f'Making directory: {self.ui['out_dir_path']}')
            Path(self.ui['out_dir_path']).mkdir(parents=True)
            print(
                f'Directory created successfully:'
                f' {Path(self.ui['out_dir_path']).is_dir()}'
            )
        else:
            try:
                if not self.ui['overwrite']:
                    raise ValueError(
                        'Output directory already exists:'
                        f" {Path(self.ui['out_dir_path']).resolve()}"
                    )
            except KeyError:
                raise ValueError(
                    'Output directory already exists:'
                    f" {Path(self.ui['out_dir_path']).resolve()}"
                )

    def process_args(self, argv):
        # Get command-line arguments
        yaml_path = ''
        if len(argv) == 0:
            segment.help(self.name, self.desc)
            sys.exit()
        if argv[0] == '-g':
            if len(argv) == 2:
                segment.generate_input_file(
                    argv[1],
                    self.name,
                    self.categorized_input_shorthands,
                    self.default_values
                )
            else:
                raise ValueError(
                    'To generate an input file, pass the path of a directory'
                    ' to save the file.'
                )
            sys.exit()
        elif argv[0] == '-h':
            segment.help(self.name, self.description)
            sys.exit()
        elif argv[0] == "-i" and len(argv) == 2:
            yaml_path = argv[1]
        if yaml_path == '':
            raise ValueError(
                f'No input file specified.'
                f' Enter "python -m segmentflow.workflow.{self.name} -h"'
                f' for more help'
            )
        return yaml_path

    def read_yaml(self):
        """Load input file and output a dictionary filled with default values
        for any inputs left blank.
        """
        # Open YAML file and read inputs, saving to self.ui
        stream = open(self.yaml_path, 'r')
        yaml_dict = yaml.load(stream, Loader=yaml.FullLoader)   # User Input
        stream.close()
        self.ui = {}
        for category, input_shorthands in self.categorized_input_shorthands.items():
            for shorthand, input in input_shorthands.items():
                # try-except to make sure each input exists in input file
                try:
                    self.ui[shorthand] = yaml_dict[category][input]
                except KeyError as error:
                    # Set missing inputs to None
                    self.ui[shorthand] = None
                finally:
                    # For any input that is None (missing or left blank),
                    # Change None to default value from default_values dict
                    if self.ui[shorthand] == None:
                        # Raise ValueError if default value listed as 'REQUIRED'
                        if self.default_values[shorthand] == 'REQUIRED':
                            raise ValueError(
                                f'Must provide value for "{input}"'
                                f' in input YAML file.'
                            )
                        else:
                            # Set default value as denoted in default_values.
                            # Value needs to be set in yaml_dict to be saved in
                            # the copy of the insput, but also in the ui dict
                            # to be used in the code
                            yaml_dict[category][input] = (
                                self.default_values[shorthand]
                            )
                            self.ui[shorthand] = self.default_values[shorthand]
                            if self.default_values[shorthand] is not None:
                                print(
                                    f'Value for "{input}" not provided.'
                                    f' Setting to default value:'
                                    f' {self.default_values[shorthand]}'
                                )
        # Change ui['out_dir_path'] to include subdirectory with name of output
        # prefix where all output files will be saved
        if Path(self.ui['out_dir_path']).stem != self.ui['out_prefix']:
            self.ui['out_dir_path'] = str(
                Path(self.ui['out_dir_path']) / self.ui['out_prefix']
            )

    def save_input_copy(self):
        # Open YAML file and read inputs
        stream = open(self.yaml_path, 'r')
        yaml_dict = yaml.load(stream, Loader=yaml.FullLoader)   # User Input
        # Save copy of YAML input file to output dir
        yaml_copy_path = (
            Path(self.ui['out_dir_path']) / f"{self.ui['out_prefix']}_input.yml"
        )
        with open(yaml_copy_path, 'w') as yaml_file:
            print(f'Saving copy of input file: {yaml_copy_path}')
            output_yaml = yaml.dump(yaml_dict, yaml_file, sort_keys=False)

