from pathlib import Path
from segmentflow import segment, view
from segmentflow.workflows.workflow import Workflow
import sys


class Labels_to_size(Workflow):
    def __init__(self, yaml_path=None, args=None):
        # Initialize parent class to set yaml_path
        super().__init__(yaml_path=yaml_path, args=args)
        self.name = Path(__file__).stem
        self.description = (
            'This workflow takes a segmented and labeled image stack and'
            ' resegments specified particles.'
            ' Developed for Segmentflow v0.0.5.'
        )
        self.categorized_input_shorthands = {
            'A. Input' : {
                'in_dir_path' : '01. Input dir path to labeled voxels',
                'file_suffix' : '02. File Suffix',
                'slice_crop'  : '03. Slice Crop',
                'row_crop'    : '04. Row Crop',
                'col_crop'    : '05. Column Crop',
                'spatial_res' : '06. Pixel size',
            },
            'B. Segmentation' : {
                'reseg_label'     : '01. Label to resegment',
                'min_peak_dist'   : '02. Min distance between seg seeds',
                'exclude_borders' : '03. Exclude border particles',
            },
            'C. Output' : {
                'out_dir_path' : '01. Output dir path',
                'out_prefix'   : '02. Output prefix',
                'save_voxels'  : '03. Save labeled voxels',
                'nslices'      : '04. Number of slices in checkpoint plots',
                'slices'       : '05. Specific slices to plot',
                'overwrite'    : '06. Overwrite files',
            },
        }
        self.default_values = {
            'in_dir_path'     : 'REQUIRED',
            'file_suffix'     : '.tif',
            'slice_crop'      : None,
            'row_crop'        : None,
            'col_crop'        : None,
            'spatial_res'     : 1,
            'reseg_label'     : 1,
            'min_peak_dist'   : 6,
            'exclude_borders' : False,
            'out_dir_path'    : 'REQUIRED',
            'out_prefix'      : '',
            'nslices'         : 3,
            'slices'          : None,
            'overwrite'       : False,
        }

    def run(self):
        """Carry out workflow (self.name) as described by self.description.
        """
        show_checkpoints = False
        checkpoint_save_dir = self.ui['out_dir_path']

        #-------------#
        # Load images #
        #-------------#
        self.logger.info(f'Beginning workflow: {workflow.name}')
        imgs_labeled = segment.load_images(
            self.ui['in_dir_path'],
            slice_crop=self.ui['slice_crop'],
            row_crop=self.ui['row_crop'],
            col_crop=self.ui['col_crop'],
            convert_to_float=False,
            file_suffix=self.ui['file_suffix'],
            logger=self.logger
        )
        fig, axes = view.plot_color_labels(
            imgs_labeled,
            nslices=3,
            exclude_bounding_slices=True,
            fig_w=7.5,
            dpi=300
        )
        fig_n = 0
        segment.output_checkpoints(
            fig,
            show=show_checkpoints,
            save_path=checkpoint_save_dir,
            fn_n=fig_n,
            fn_suffix='labeled-imgs'
        )

        #--------------------------#
        # Resegment isolated label #
        #--------------------------#
        self.logger.info(
            f"Resegmenting voxels with label {self.ui['reseg_label']}...")
        imgs_resegmented = segment.watershed_segment(
            imgs_labeled==self.ui['reseg_label'],
            min_peak_distance=self.ui['min_peak_dist'],
            exclude_borders=self.ui['exclude_borders'],
            return_dict=False
        )
        # Generate instance label viz
        fig, axes = view.color_labels(
            imgs_resegmented,
            slices=self.ui['slices'],
            nslices=self.ui['nslices'],
            fig_w=7.5,
            dpi=300
        )
        fig_n += 1
        segment.output_checkpoints(
            fig, show=show_checkpoints, save_path=checkpoint_save_dir,
            fn_n=fig_n, fn_suffix='instance-seg-imgs')

        #-------------#
        # Save voxels #
        #-------------#
        if self.ui['save_voxels']:
            segment.save_images(
                imgs_resegmented,
                Path(self.ui['out_dir_path']) / f"{self.ui['out_prefix']}_labeled_voxels"
            )

if __name__ == '__main__':
    print()
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print('Welcome to Segmentflow!')
    print('~~~~~~~~~~~~~~~~~~~~~~~')
    print()
    # Pass path to YAML from args and store in workflow class
    workflow = Labels_to_size(args=sys.argv[1:])
    # Load input data from YAML and store as UI attribute
    workflow.read_yaml()
    # Create and store a logger object as an attribute for saving a log file
    workflow.create_logger()
    try:
        workflow.run()
    except Exception as error:
        workflow.logger.exception(error)
    print()
    print('~~~~~~~~~~~~~~~~~~~~~')
    print('Successful Completion')
    print('~~~~~~~~~~~~~~~~~~~~~')
    print()
